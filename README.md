# Gulp Boilerplate

Boilerplate for Gulp project

## Getting Started

### 1. Install gulp globally:

```sh
$ npm install --global gulp
```

### 2. Install gulp in your project devDependencies:

```sh
$ npm install --save-dev gulp
```

## Create your project:

```
git clone https://niponk@bitbucket.org/niponk/gulp-boilerplate.git my-app
```

### Run it

1. `$ cd my-app`
2. `$ npm install`
3. `$ gulp`
