'use strict'

import gulp from 'gulp'
import gulpLoadPlugins from 'gulp-load-plugins'
import browserSync from 'browser-sync'
import runSequence from 'run-sequence'

const $ = gulpLoadPlugins()
const reload = browserSync.reload

gulp.task('image', () => {
  return gulp
    .src('./src/img/**/*')
    .pipe($.imagemin())
    .pipe(gulp.dest('./dist/public/img'))
})

gulp.task('pug', () => {
  return gulp
    .src('./src/pug/**/*.pug')
    .pipe($.pug({ pretty: true }))
    .pipe(gulp.dest('./dist'))
})

gulp.task('scss', () => {
  return gulp
    .src('./src/scss/**/*.scss')
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer())
    .pipe($.cssnano())
    .pipe($.rename('style.min.css'))
    .pipe(gulp.dest('./dist/public/css'))
})

gulp.task('js', () => {
  return gulp
    .src('./src/js/**/*.js')
    .pipe($.babel())
    .pipe($.concat('bundle.min.js'))
    .pipe($.uglify())
    .pipe(gulp.dest('./dist/public/js'))
})

gulp.task('watch', () => {
  gulp.watch(['./src/pug/**/*.pug'], ['pug', reload])
  gulp.watch(['./src/scss/**/*.scss'], ['scss', reload])
  gulp.watch(['./src/js/**/*.js'], ['js', reload])
})

gulp.task('serve', () => {
  browserSync({
    server: './dist',
  })
})

gulp.task('compile', (cb) => {
  runSequence('pug', 'scss', 'js', 'image', cb)
})

gulp.task('default', (cb) => {
  runSequence('compile', ['serve', 'watch'], cb)
})
